require 'test_helper'

class RoutesTest < ActionController::IntegrationTest
  test "should route to tasks" do
    assert_routing "/api/tasks", { format: :json, controller: "api/tasks", action: "index" }
  end

  test "should create a new task" do
    assert_routing({ method: :post, path: '/api/tasks' }, { format: :json, controller: 'api/tasks', action: 'create' })
  end

  test "should update a task" do
    assert_routing({ method: :patch, path: '/api/tasks/1' }, { format: :json, controller: 'api/tasks', action: 'update', id: '1' })
    assert_routing({ method: :put, path: '/api/tasks/1' }, { format: :json, controller: 'api/tasks', action: 'update', id: '1' })
  end

  test "should delete a task" do
    assert_routing({ method: :delete, path: '/api/tasks/1' }, { format: :json, controller: 'api/tasks', action: 'destroy', id: '1' })
  end
end
