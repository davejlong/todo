require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  test "ordered by due_date by default" do
    actual = Task.all.map { |task| task.due_date }
    expected = [
      Date.new(2013, 10, 11),
      Date.new(2013, 10, 12),
      Date.new(2013, 10, 15)
    ]
    assert_equal expected, actual
  end

  test "description required" do
    assert Task.create.invalid?

    assert Task.create(description: 'Something').valid?
  end

  test "completed defaults to false" do
    refute_nil Task.new.completed
    refute Task.new.completed
  end
end
