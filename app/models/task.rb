class Task < ActiveRecord::Base
  validates :description, presence: true

  default_scope { order(:due_date) }
end
