class Api::TasksController < ApplicationController
  def index
    render json: Task.find(:all).tasks
  end

  def create
  end

  def update
  end

  def destroy
  end
end
