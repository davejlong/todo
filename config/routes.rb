Todo::Application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resources :tasks, only: [ :index, :create, :update, :destroy ]
  end
end
